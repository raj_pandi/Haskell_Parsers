# Infoblox Log File Parser

# Requirement
GHCi, version 8.2.1: http://www.haskell.org/ghc/  :? for help

Windows 10 64 bit

# Imports from Hackage 

import Data.Word  

import Data.Time

import Data.Traversable

import Data.Attoparsec.Text as AT

import qualified Data.ByteString as B

import Data.Text (Text)

import Control.Applicative

import System.Environment

import qualified Data.Text    as Text

import qualified Data.Text.IO as Text

# Usage 

> ghc -o info_parse info_parse.hs

This will compile to info_parse.exe on Windows platform

# Input
Generally infoblox filename will be downloaded as sysLog.tar. Once extracted, the log file will be on
sysLog->messages. The filename "messages" will be the input (i.e input log file)to the program.

Usage:
> info_parse.exe [INPUT_LOG_FILE]


# Queryable Output
Done "" (LogEntry {entryTime = 2017-12-20 15:21:12,ip_info = IP * * * * port_name, domain_info = "www.google.com"})

Note:The entry query info is same as domain info except it contains the ip address of the domain name

For security reasons, ip and domain informations are hidden.

# Reference
https://www.schoolofhaskell.com/school/starting-with-haskell/libraries-and-frameworks/text-manipulation/attoparsec