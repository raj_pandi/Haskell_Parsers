-- |  Copyright 2017  [Rajkumar Pandi]

-- |  Licensed under the Apache License, Version 2.0 (the "License");
-- |  you may not use this file except in compliance with the License.
-- |  You may obtain a copy of the License at

-- |      http://www.apache.org/licenses/LICENSE-2.0

-- |  Unless required by applicable law or agreed to in writing, software
-- |  distributed under the License is distributed on an "AS IS" BASIS,
-- |  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- |  See the License for the specific language governing permissions and
-- |  limitations under the License.
   
{-# LANGUAGE OverloadedStrings #-}

import Data.Word
import Data.Time
import Data.Traversable
import Data.Attoparsec.Text as AT
import qualified Data.ByteString as B
import Data.Text (Text)
import Control.Applicative
import qualified Data.Text    as Text
import qualified Data.Text.IO as Text


-- | Type for IP's.
data IP = IP Word8 Word8 Word8 Word8 Int deriving Show

-- | Type for Daemon or Syslog
data MsgLogType = Daemon | Syslog deriving Show

-- | Type for nameservers
data Domain = NS1 | NS2 deriving Show

-- | Type for Nametype
data MsgLog = SYSLOGNG | NAMED | DHCPD deriving Show

-- | Type for Clientinfo
data Clientinfo = CLIENTINFO deriving Show

-- | Type for all columns in the log
data LogEntry =
  LogEntry { entryTime :: LocalTime
           , messageLogService  :: MsgLogType, named_server :: Domain, messageLogName :: MsgLog, ip_info :: IP, domain_info :: Text, query :: Text} deriving Show
	 
-- | Parser of values of type 'IP'.
parseIP :: Parser IP
parseIP = do
  d1 <- decimal
  char '.'
  d2 <- decimal
  char '.'
  d3 <- decimal
  char '.'
  d4 <- decimal
  char '#'
  d5 <- decimal

  return $ IP d1 d2 d3 d4 d5 
  
 
-- | Parser of values of type 'LocalTime'.
timeParser :: Parser LocalTime
timeParser = do
  y  <- count 4 digit
  char '-'
  mm <- count 2 digit
  char '-'
  d  <- count 2 digit
  char 'T'
  h  <- count 2 digit
  char ':'
  m  <- count 2 digit
  char ':'
  s  <- count 2 digit
  char '-'
  hh <- count 2 digit
  char ':'
  uu <- count 2 digit
  return $
    LocalTime { localDay = fromGregorian (read y) (read mm) (read d)
              , localTimeOfDay = TimeOfDay (read h) (read m) (read s)
                }

-- | Parser of values of type Message Log Services
productParser :: Parser MsgLogType
productParser =
     (string "daemon"    >> return Daemon)
 <|> (string "syslog" >> return Syslog)

-- | Parser of values of type Named Server
domainParser :: Parser Domain
domainParser =
     (string "*.somedomain.com" >> return NS1)
 <|> (string "*.somedomain.com" >> return NS2)

-- | Parser of values of type Message Log Services
nameParser :: Parser MsgLog
nameParser =
     (string "named[27120]:" >> return NAMED)
 <|> (string "syslog-ng[11050]:" >> return SYSLOGNG)
 <|> (string "dhcpd[28043]:" >> return DHCPD)
 
clientParser :: Parser Clientinfo
clientParser =
     (string "info client" >> return CLIENTINFO) 

-- | Parser of values of type Domain Name	 
urlParser :: Parser Text
normal =  AT.takeWhile (/= ')')
urlParser = do string "("
               res <- normal
               string "):"
               return $ res
  
-- | Parser of values of type query
queryParser ::  Parser Text
qy =  AT.takeWhile (/= ')')
queryParser = do string "query:"
                 rest <- qy
                 string ")"
                 return $ rest

				 
logEntryParser :: Parser LogEntry
logEntryParser = do
  t <- timeParser
  char ' '
  p <- productParser
  char ' '
  d <- domainParser
  char ' '
  n <- nameParser
  char ' '
  c <- clientParser
  char ' '
  ip <- parseIP
  char ' '
  hn <- urlParser
  char ' '
  qp <- queryParser
  return $ LogEntry t p d n ip hn qp
 
length' :: (Num b) => [a] -> b 
length' [] = 0 
length' xs = sum [1 | _ <- xs]

main :: IO ()
main = do
  input_file <- getArgs
  ls <- fmap Text.lines (Text.readFile (input_file!!0))
  let lenlist = length' ls
  logmap <- forM [1..lenlist] $ \number -> do 
    print $ parse logEntryParser (ls!!number)
  print logmap

 
